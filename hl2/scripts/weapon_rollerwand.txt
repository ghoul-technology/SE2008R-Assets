// Slam

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"	"Rollerwand"
	"viewmodel"			"models/weapons/v_slam.mdl"
	"playermodel"			"models/weapons/w_slam.mdl"
	"anim_prefix"			"slam"
	"bucket"			"6"
	"bucket_position"		"3"

	"clip_size"				"1"
	"primary_ammo"			"None"
	"secondary_ammo"		"None"
	"default_clip2"			"1"

	"weight"				"0"
	"item_flags"			"0"

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"WeaponIcons"
				"character"	"o"
		}
		"weapon_s"
		{
				"font"		"WeaponIconsSelected"
				"character"	"o"
		}
		"ammo"
		{
				"font"		"WeaponIcons"
				"character"	"o"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"48"
				"y"			"72"
				"width"		"24"
				"height"	"24"
		}
	}
}