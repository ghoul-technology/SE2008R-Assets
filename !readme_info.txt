PACKAGED BY JUEST ZUNGO

PRIVATE DISTRIBUTION

IMPORTANT: IN A EMPTY FOLDER CREATE A FOLDER CALLED GAME AND EXTRACT THERE (because of content folder and src publishes to game)

For those who want to compile this on your own, get the leak, 2005 with sp1 and vista update with platform sdk is optimal. vs2008 needs bumping min target osto a valid nt version; compiler stalls, 2010: make sure to get rid of VPC since it's incompatible (it's src is available tho) , VPC scripts need fix because of file format. Xbox 360 XDK 6534 (add last to paths), directx 8 sdk (add after winsdk to paths), directx 9 sdk is partially included, optional install from ms, also add after winsdk to paths.

Targets Windows 9x particularly because vmpi_job_search requires and makes use of MFC support for Internet Explorer 4 Common Controls
libparsifal-0.8.3.zip found in devtools\bin, convert from vs6, needs to be extracted and placed in root

Lua, havana, videocardstats, phonemeextractor, sfmgen, sfmobjects, soundsystem, miles, vphysics missing
my copy of repo is on vs 2005, includes DX8.1 include and lib, parsifal is extracted, fixed missing mxtk, added a working soundsystem.dll, files required to run the game, must go to game\bin.

Compiled experimental early 2008 revision of Source 2007 based on the 2012 leak:
---------------------------------------------------------------------------------

Pre-SteamPipe, Steam optional (copy steam binaries from SDK Base 2007 and steamappid.txt required), Similar to SDK Base 2007 and Orange Box original Steam envirorment
SDK not broken unlike 2013
NO VR NOR VPK SUPPORT, Game must be extracted and gameinfo.txt downgraded. also move motd, mapcycle from cfg to root and remove _default from filename
shipping proper gameinfo.txt (some with experiments) and (old) steam.inf
avoid using pre-orangebox resources or stuff introduced in 2013 since some mess up dialogs

white/black screens = wrong or missing shaders or conflict with a Overlay or vidcfg.bin issue
Shader compile was mid-complete, finished the job without cleanup, yet there's still missing shaders. Comparable to Source 2007 Binaries Depot 216 included in SDK Base 2007.
take launcher folder from Source 2007 or orangebox in Source SDK to use SDK Launcher; -game launcher (folder for vconfig included)
Steam/Retail/OrangeBox Platform dir required.
-tools working, needs enginetools.txt pointing to tools dlls. (all tools working except foundry which is almost working, crashes on map load after loading models)
Engine and SDK uses ../content/<game> for cubemap temp and engine tools

32 slots for all games
10 weapon slots. (no more unused slot commands)
AmmoTypes count increased and more ammotypes added and adapted hl2 and episodic skill.cfg
npc_blob included in hl2 and episodic (may not work as expected, symptoms include vertical movement and not following the npc entity when it moves)
weapons: sniperrifle, flaregun, irifle, manhack working out of box with missing models. Shipped weapon scripts from the beta leak and patched up to have crosshair font instead of texture which is blurrier. 
bullsquid and houndeye entity class references uncommented. bullsquid depends on grenade_spit, working out of box and included in hl2 and episodic.
built-in MP3 Player enabled
Scenes.image enforcement removed (able to load both from image and VCDs)
[solved by commenting or defining] There's no definition for NO_MULTIPLE_CLIENTS. this means you can run multiple instances. prepare for unforeseen consequences due to inter-engine communication (includes hammer when launched via hl2 -editor)
Games are orangebox updates: copy from orangebox disc or old backup is strongly recommended for hl2, ep1 and ep2, best is a very-early-2008 backup of all games (GCF data)
OB portal, hl2, episodic (ep1), ep2, TF2 1.0.1.8 2008/02/14, lostcoast, orangebox dods and css, test_hardware working (broken in sdk 2013)
hl1 is actually hldms not hls and hybrid!, crowbar was originally missing.
OB disc PCFs seem to be incompatible or corrupt because it can't find the particle functions because of a unimplemented particle render operation.
sourcetest working only if you provide steam_api.dll from sdk base 2007, same with mods, although that's more complex because client.dll falls back due to The procedure entry point Plat_SetBenchmarkMode could not be located in the dynamic link library tier0.dll. which causes the whole thing to crash!

Tested with:
mix of 2004 and 2007 hl2
mix of 2006 and 2007 ep1
2006 ep1 disc
orangebox disc
2005 lostcoast
old engine hl(dm)s
dods, hl2dm, from retail
css v34
2008 and 2011 tf2 which didn't work well
mods: garrysmod 9 (doesn't work properly because its for old engine), missing information (Team GabeN), City 17: Episode 1 (MrTwo), Obsidian Conflict